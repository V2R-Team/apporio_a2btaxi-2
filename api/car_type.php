<?php

include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

function sortByOrder($a, $b) 
{
      return $a['distance'] - $b['distance'];
}

$city=$_REQUEST['city'];
$user_lat=$_REQUEST['user_lat'];
$user_long=$_REQUEST['user_long'];
$language_id=$_REQUEST['language_id'];

if($city!= "" && $user_lat!= "" && $user_long!= "")
{
		$query="SELECT * FROM rate_card INNER JOIN car_type ON rate_card.car_type_id=car_type.car_type_id WHERE rate_card.city_id =3";
	        $result = $db->query($query);
		$list=$result->rows;
	
		if(!empty($list))
		{
			$c=array();
			$d=array();
			
			foreach($list as $login)
			{
				$car_type_id=$login['car_type_id'];
                                $car_type_name=$login['car_type_name'];
                                $car_type_image=$login['car_type_image'];
                                $city_id=$login['city_id'];
                                $base_fare=$login['base_price_miles'];
                                
				$query1="select * from driver where car_type_id='$car_type_id' and login_logout=1 and busy=0 and online_offline=1 and status=1";
				$result1 = $db->query($query1);
				$list1=$result1->rows;
				
					$info 	= $_REQUEST;
        unset($info['PHPSESSID']);
			
				if(!empty($list1))
				{
				foreach($list1 as $login3)
        		{
                	$driver_lat = $login3['current_lat'];
                	$driver_long =$login3['current_long'];
					
					//echo "lat  ".$driver_lat;
					//echo "long  ".$driver_long;
					
					$theta = $user_long - $driver_long;
  					$dist = sin(deg2rad($user_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($user_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
  					$dist = acos($dist);
  					$dist = rad2deg($dist);
  					$miles = $dist * 60 * 1.1515;
  					$unit = strtoupper("M");
  					if ($unit == "K") 
  					{
						$km=$miles* 1.609344;
  					} 
  					else if ($unit == "N") 
  					{
      		        	$miles * 0.8684;
  					}	 
  					else if($unit == "M")
  					{
	  					$miles;
						//echo "miles		".$miles;
  					}
					if($miles<= 10)
					{
						$c[] = array("driver_id"	=> $login3['driver_id'],"driver_lat"	=> $login3['current_lat'],"driver_long"	=> $login3['current_long'],"distance" => $miles,);
						
						//echo "less tha 10 k ander a gya";
					}
				}
				usort($c, 'sortByOrder');
		        $lati = $c[0]['driver_lat'];
				$longi = $c[0]['driver_long'];
				
			    $from 		= $user_lat.",".$user_long;
			    $to 		= $lati.",".$longi;
			    $data 	= file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=en-EN&key=AIzaSyCTAg2TXt8AiswL_Ro6k76c_p1bisIWxEU");
			    $data 		= json_decode($data, true);
				$status=$data['rows'][0]['elements'][0]['status'];
				
				if($status=="OK")
				{
					$dist 		= $data['rows'][0]['elements'][0]['distance']['text'];
					$dist_inval 	= $data['rows'][0]['elements'][0]['distance']['value'];
					$duration 	= $data['rows'][0]['elements'][0]['duration']['text'];
				
                	$c=array();
					$d[]=array('car_type_id'=>$car_type_id,'car_type_name'=>$car_type_name,'car_type_image'=>$car_type_image,'city_id'=>$city_id,'distance'=>$duration,'base_fare'=>$base_fare);
				}
				else{
					$d[]=array('car_type_id'=>$car_type_id,'car_type_name'=>$car_type_name,'car_type_image'=>$car_type_image,'city_id'=>$city_id,'distance'=>"",'base_fare'=>$base_fare);
				}
				}
				else{
			$d[]=array('car_type_id'=>$car_type_id,'car_type_name'=>$car_type_name,'car_type_image'=>$car_type_image,'city_id'=>$city_id,'distance'=>"",'base_fare'=>$base_fare);
				}
			}
		}
			
	if(!empty($d))
	{
			$re = array('result'=> 1,'msg'	=> $d,);
	}
	else
	{
		 $language="select * from messages where language_id='$language_id' and message_id=6";
	                        $lang_result = $db->query($language); 
                                $lang_list=$lang_result->row;

                                $message_name=$lang_list['message_name'];
	$re = array('result' => 0,'msg'	=>$message_name);
	}
		
}
else 
{
	 $language="select * from messages where language_id='$language_id' and message_id=3";
	                        $lang_result = $db->query($language); 
                                $lang_list=$lang_result->row;

                                $message_name=$lang_list['message_name'];
	$re = array('result' => 0,'msg'	=>$message_name);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>
