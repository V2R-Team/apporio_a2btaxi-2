<?php

include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

$user_id=$_REQUEST['user_id'];
$coupon_code=$_REQUEST['coupon_code'];
$pickup_lat=$_REQUEST['pickup_lat'];
$pickup_long=$_REQUEST['pickup_long'];
$pickup_location=$_REQUEST['pickup_location'];
$drop_lat=$_REQUEST['drop_lat'];
$drop_long=$_REQUEST['drop_long'];
$drop_location=$_REQUEST['drop_location'];
$later_date=$_REQUEST['later_date'];
$later_time=$_REQUEST['later_time'];
$ride_type=$_REQUEST['ride_type'];
$ride_status=$_REQUEST['ride_status'];
$car_type_id=$_REQUEST['car_type_id'];
$language_id=$_REQUEST['language_id'];
$payment_option_id=$_REQUEST['payment_option_id'];
$card_id=$_REQUEST['card_id'];

if($user_id!="" && $pickup_lat!="" && $pickup_long!="" && $pickup_location!="" && $drop_lat!="" &&
$drop_long!="" && $drop_location!="" && $ride_type!="" && $ride_status!="" && $later_date!="" && $later_time!="" && $car_type_id!="" && $payment_option_id!="") 
{
	$dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
	$data=$dt->format('M j'); 
	$day=date("l");
	$date=$day.", ".$data ;
	$time=date("h:i A");
	$last_time_stamp = date("Y-m-d h:i:sA");
	$query1="INSERT INTO ride_table (user_id,coupon_code, pickup_lat, pickup_long,pickup_location,drop_lat,drop_long,drop_location,
	ride_date,ride_time,ride_type,ride_status,later_date,later_time,car_type_id,status,payment_option_id,card_id,last_time_stamp) 
	VALUES('$user_id','$coupon_code','$pickup_lat','$pickup_long','$pickup_location','$drop_lat','$drop_long','$drop_location',
	'$date','$time','2','$ride_status','$later_date','$later_time','$car_type_id','1','$payment_option_id','$card_id','$last_time_stamp')";
	
	$db->query($query1);
	$last_id = $db->getLastId();
	$query3="select * from ride_table where ride_id='$last_id'";
	$result3 = $db->query($query3);
	$list=$result3->row;
	 $language="select * from messages where language_id='$language_id' and message_id=29";
	                        $lang_result = $db->query($language); 
                                $lang_list=$lang_result->row;

                                $message=$lang_list['message_name'];
	$re = array('result'=> 1,'msg'=> $message,'details'	=> $list);	
}
else
{
   $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>