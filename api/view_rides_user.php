<?php

include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

$user_id=$_REQUEST['user_id'];
$language_id=$_REQUEST['language_id'];

if($user_id!="") 
{
	$query="select * from ride_table where user_id='$user_id' order by ride_id desc";
	$result = $db->query($query);
	$list=$result->rows;
	foreach($list as $login)
	{
		$ride_id=$login['ride_id'];
     	$query1="select * from done_ride where ride_id='$ride_id'";
     	$result1 = $db->query($query1);
     	$ex_rows1=$result1->num_rows;
	 
 		if($ex_rows1==0)
		{
			$user_id=$login['user_id'];
     		$pickup_lat=$login['pickup_lat'];
     		$pickup_long=$login['pickup_long'];
     		$pickup_location=$login['pickup_location'];
     		$drop_lat=$login['drop_lat'];
     		$drop_long=$login['drop_long'];
     		$drop_location=$login['drop_location'];
     		$ride_date=$login['ride_date'];
     		$ride_time=$login['ride_time'];
			
			$later_date = $login['later_date'];
     		$later_time = $login['later_time'];
			
            $ride_status=$login['ride_status'];
            $ride_type=$login['ride_type'];
	        $driver_id=$login['driver_id'];
			$car_type_id=$login['car_type_id'];
			
			$query44="select * from car_type where car_type_id='$car_type_id'";
			$result44 = $db->query($query44);
			$list44=$result44->row;
			$car_type_name=$list44['car_type_name'];
			$car_type_image=$list44['car_type_image'];
			
     		$begin_lat="";
     		$begin_long="";
     		$begin_location="";
     		$end_location="";
     		$arrived_time="";
     		$begin_time="";
     		$payment_status="0";
            $amount="";
            $distance="";
            $time="";
     
    		$c[]=array('ride_id'=>$ride_id,'user_id'=>$user_id,'pickup_lat'=>$pickup_lat,'pickup_long'=>$pickup_long,'drop_lat'=>$drop_lat,'drop_long'=>$drop_long,'pickup_location'=>$pickup_location,'drop_location'=>$drop_location,'ride_date'=>$ride_date,'ride_time'=>$ride_time,'later_date'=>$later_date,'later_time'=>$later_time,'driver_id'=>$driver_id,'ride_status'=>$ride_status,'ride_type'=>$ride_type,'begin_lat'=>$begin_lat,'begin_long'=>$begin_long,'arrived_time'=>$arrived_time,'begin_time'=>$begin_time,'payment_status'=>$payment_status,'begin_location'=>$begin_location,'end_location'=>$end_location,'amount'=>$amount,'distance'=>$distance,'time'=>$time,'car_type_name'=>$car_type_name,'car_type_image'=>$car_type_image,);
		}
		else
		{
			$list123=$result1->row;
			$user_id=$login['user_id'];
     		$pickup_lat=$login['pickup_lat'];
     		$pickup_long=$login['pickup_long'];
     		$pickup_location=$login['pickup_location'];
     		$drop_lat=$login['drop_lat'];
     		$drop_long=$login['drop_long'];
     		$drop_location=$login['drop_location'];
     		$ride_date=$login['ride_date'];
     		$ride_time=$login['ride_time'];
			$later_date = $login['later_date'];
     		$later_time = $login['later_time'];
     		$ride_status=$login['ride_status'];
			$ride_type=$login['ride_type'];
			$driver_id=$login['driver_id'];
			$car_type_id=$login['car_type_id'];
			
		$query44="select * from car_type where car_type_id='$car_type_id'";
			$result44 = $db->query($query44);
			$list44=$result44->row;
			$car_type_name=$list44['car_type_name'];
			$car_type_image=$list44['car_type_image'];
			
			
			
			
     		$begin_lat=$list123['begin_lat'];
     		$begin_long=$list123['begin_long'];
     		$begin_location=$list123['begin_location'];
     		$end_location=$list123['end_location'];
     		$arrived_time=$list123['arrived_time'];
     		$begin_time=$list123['begin_time'];
     		$payment_status=$list123['payment_status'];
            $amount=$list123['amount'];
            $distance=$list123['distance'];
            $time=$list123['tot_time'];

            $c[]=array('ride_id'=>$ride_id,'user_id'=>$user_id,'pickup_lat'=>$pickup_lat,'pickup_long'=>$pickup_long,'drop_lat'=>$drop_lat,'drop_long'=>$drop_long,'pickup_location'=>$pickup_location,'drop_location'=>$drop_location,'ride_date'=>$ride_date,'ride_time'=>$ride_time,'later_date'=>$later_date,'later_time'=>$later_time,'driver_id'=>$driver_id,'ride_status'=>$ride_status,'ride_type'=>$ride_type,'begin_lat'=>$begin_lat,'begin_long'=>$begin_long,'arrived_time'=>$arrived_time,'begin_time'=>$begin_time,'payment_status'=>$payment_status,'begin_location'=>$begin_location,'end_location'=>$end_location,'amount'=>$amount,'distance'=>$distance,'time'=>$time,'car_type_name'=>$car_type_name,'car_type_image'=>$car_type_image,);
		}
	}
	if(!empty($c))
    {
		$re = array('result'=> 1,'msg'	=> $c,);
    }
    else
    {
		 $language="select * from messages where language_id='$language_id' and message_id=6";
	                        $lang_result = $db->query($language); 
                                $lang_list=$lang_result->row;

                                $message_name=$lang_list['message_name'];
	$re = array('result' => 0,'msg'	=>$message_name);
    }
}
else
{
	$language="select * from messages where language_id='$language_id' and message_id=3";
	                        $lang_result = $db->query($language); 
                                $lang_list=$lang_result->row;

                                $message_name=$lang_list['message_name'];
	$re = array('result' => 0,'msg'	=>$message_name);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>