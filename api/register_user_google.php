<?php
require("Mail.php");
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

$social_token=$_REQUEST['social_token'];
$user_name=$_REQUEST['user_name'];
$user_email=$_REQUEST['user_email'];
$user_phone=$_REQUEST['user_phone'];
$social_type=$_REQUEST['social_type'];
$language_id=$_REQUEST['language_id'];

if($social_token!="" && $user_name!="" && $user_phone!="" && $social_type!="")
{
	$query="select * from user where user_phone='$user_phone'";
	$result = $db->query($query);
	$ex_rows=$result->num_rows;
	
	$dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
	$data=$dt->format('M j'); 
	$day=date("l");
	$date=$day.", ".$data ;
	if($user_email != "")
	{
	  $admin_email = $user_email ;
		$recipients = $admin_email;
		$headers['From']    = '"Taxi App" <taxiapp@apporio.co.uk>';
		$headers['To']      = '"Taxi App" <taxiapp@apporio.co.uk>';
		$headers['Subject'] = 'Taxi App Registration';
		$headers["Content-Type"] = "text/html; charset=UTF-8";  
			
		$body = '<html>
			<head>
				<title>'.$subject.' </title>
			</head>
			<body>
				<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="padding:24px;background-color:#34495e">
					<tbody>
						<tr>
							<td>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<span class="HOEnZb"><font color="#888888"> </font></span>
								<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="padding:30px 30px 30px 30px;background-color:#fafafa">
									<tbody>
									
									<tr>
										<td  style="font-size:28px;font-family:Arial,Helvetica,sans-serif;color:#34495e; text-align:center; padding-bottom:30px;" ><b> Taxi App</b></td>
									</tr>
									<tr>
										<td style="font-size:12px;font-family:Arial,Helvetica,sans-serif;color:#34495e" colspan="2"> Dear <strong>'.$user_name.'</strong>, </td>
									</tr>
									<tr>
										<td height="10" colspan="2"> </td>
									</tr>
									<tr>
									
										<td style="font-size:12px;text-align:justify;line-height:1.4;font-family:Arial,Helvetica,sans-serif;color:#34495e" colspan="2">	 	 
										<strong> We are happy to inform you that your registration for Taxi App is Succesfull. We assure you of our best services at all times .
 </strong>
										<br>
										
										
										</td>
									</tr>
									<tr>
										<td height="10" colspan="2"> </td>
									</tr>
									<tr>
										<td height="24" colspan="2">
											We Hope you enjoyed your experience using our service.
										</td>
									</tr>
									
									<tr>
										<td height="30" style="border-bottom:1px solid #eaedef" colspan="2"> </td>
									</tr>
									<tr>
										<td height="12" colspan="2"> </td>
									</tr>
									<tr>
										<td colspan="3" align="center">
											<span class="HOEnZb"> <font color="#888888"> </font> </span>
											<span class="HOEnZb"> <font color="#888888"> </font> </span>
											<table width="100%">
												<tbody>
													<tr>
														<td align="center">
															Thanks & Regards,
															<br> <br>
															The Taxi App Team.
														</td>
													</tr>
												</tbody>
											</table>
											<span class="HOEnZb"><font color="#888888"> </font> </span>
										</td>
									</tr>
								</tbody>
							</table>
							<span class="HOEnZb"><font color="#888888"> </font></span>
						</td>
					</tr>
				</tbody>
			</table>
			</body>
		</html>';
		
		$params['sendmail_path'] = '/usr/lib/sendmail';
		$mail_object =& Mail::factory('sendmail', $params);
		$mail_object->send($recipients, $headers, $body);
	}else{
	}
		
		
			
	if($ex_rows==1)
	{
		if($social_type==1)
		{
			$query2="UPDATE user SET user_name='$user_name', user_email='$user_email', google_token='$social_token', email_verified=1,phone_verified=1, login_logout=1 WHERE user_phone='$user_phone'";
			
			$db->query($query2);
			$query3="select * from user where user_phone='$user_phone'";
			$result3 = $db->query($query3);
			$list=$result3->row;
			$re = array('result'=> 1,'msg'=> "Register Succesfully",'details'	=> $list);		
		}
		else 
		{

			$query2="UPDATE user SET user_name='$user_name', user_email='$user_email', facebook_token='$social_token', email_verified=1,phone_verified=1, login_logout=1 WHERE user_phone='$user_phone'";
			
			$db->query($query2);

			$query3="select * from user where user_phone='$user_phone'";
			$result3 = $db->query($query3);
			$list=$result3->row;
			$re = array('result'=> 1,'msg'=> "Register Succesfully",'details'	=> $list);	
		}	
	}
	else
	{
		if($social_type==1)
		{
			$query2="INSERT INTO user (user_name, user_email, user_phone, google_token, register_date,email_verified,phone_verified,login_logout,status) VALUES('$user_name','$user_email','$user_phone','$social_token','$date',1,1,1,1)";
			
			$db->query($query2);
			
			$last_id = $db->getLastId();
			$query3="select * from user where user_id='$last_id'";
			$result3 = $db->query($query3);
			$list=$result3->row;
			$re = array('result'=> 1,'msg'=> "Register Succesfully",'details'	=> $list);
		}
		
		else
		{
			$query2="INSERT INTO user (user_name, user_email, user_phone, facebook_token, register_date,email_verified,phone_verified,login_logout,status) VALUES('$user_name','$user_email','$user_phone','$social_token','$date',1,1,1,1)";
			
			$db->query($query2);
			
			$last_id = $db->getLastId();

			$query3="select * from user where user_id='$last_id'";
			$result3 = $db->query($query3);
			$list=$result3->row;
			$re = array('result'=> 1,'msg'=> "Register Succesfully",'details'	=> $list);
		}	
	}
}
else
{
   $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>