<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['COMPANY']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$query="select * from company_payment WHERE company_id='".$_SESSION['COMPANY']['ID']."'";
$result = $db->query($query);
$list = $result->row;


?>
<link rel="stylesheet" type="text/css" href="card.css">
<script>
    function validatelogin() {
        var cardno = document.getElementById('cardno').value;
        var cvv = document.getElementById('cvv').value;
        if(cardno == "")
        {
            alert("Enter Credit Card Number");
            return false;
        }
        if(cvv == "")
        {
            alert("Enter CVV");
            return false;
        }
    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Make Payment With Brain Tree</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class=" form" >
                        <div id="main">
                            <form action="DoDirectPayment.php" method="POST" enctype="multipart/form-data"  onSubmit="return validatelogin()">
                                <div id="container">
                                    <h2>Pay with Debit or Credit card</h2>
                                    <hr/>


                                    <table style="width:100%">
                                        <tr>

                                            <td id="td-label">Card number : </td>
                                            <td><input type="text" name="creditCardNumber" id="cardno" placeholder="enter card number" maxlength="20"></td>

                                        </tr>

                                        <tr>

                                            <td id="td-label">Expiry date : </td>
                                            <td><div id="date-div"><select name="expDateMonth">
                                                        <option value="01">01</option>
                                                        <option value="02">02</option>
                                                        <option value="03">03</option>
                                                        <option value="04">04</option>
                                                        <option value="05">05</option>
                                                        <option value="06">06</option>
                                                        <option value="07">07</option>
                                                        <option value="08">08</option>
                                                        <option value="09">09</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select></div>
                                                <div id="year-div">

                                                    <select name="expDateYear">
                                                        <option value="2017">2017</option>
                                                        <option value="2018">2018</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2020">2020</option>
                                                        <option value="2021">2021</option>
                                                        <option value="2022">2022</option>
                                                        <option value="2023">2023</option>
                                                        <option value="2024">2024</option>
                                                    </select>
                                                </div></td>

                                        </tr>

                                        <tr>

                                            <td id="td-label">CVV : </td>
                                            <td><div id="date-div"><input type="text" name="cvv2Number" id="cvv" placeholder="cvv" maxlength="4"></div></td>

                                        </tr>
                                        <tr>

                                            <td id="td-label">Amount( USD ) : </td>
                                            <td><input type="text" name="amount" id="name" placeholder="enter amount " value="<?php echo $list['payment'];?>" readonly></td>

                                        </tr>
                                    </table>

                                    <br>
                                    <center><input  style="  width: 20%;"type="submit" id="buynow" name="DoDirectPaymentBtn" value="Pay Now"></center><br>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</body>
</html></body></html>