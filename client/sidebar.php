<?php 
include_once '../apporioconfig/connection.php';
 ?>
<aside class="left-panel"> 
  
  <!-- brand -->
  <div class="logo"> <a href="home.php?pages=dashboard" class="logo-expanded"> <img src="../images/logo.png" width="65" height="65" alt="logo"> <span class="nav-label">A2B</span> </a> </div>
  <!-- / brand --> 
  
  <!-- Navbar Start -->
  <nav class="navigation">
    <ul class="list-unstyled">
      <!-- Dashboard Start -->
      <?php
        $li_open = $arr_open = $ul_open = "";
        if(empty($_REQUEST['pages'])) {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=dashboard"><i class="glyphicon glyphicon-home" aria-hidden="true"></i> <span class="nav-label">Dashboard</span><span class="selected"></span></a></li>

        <!-- Driver -->
        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "add-driver" || $_REQUEST['pages'] == "view-driver") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="javascript:;"><i class="fa fa-users"></i> <span class="nav-label">Drivers</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
            <ul class="list-unstyled">
                <li><a href="home.php?pages=add-driver">Add Driver</a></li>
                <li><a href="home.php?pages=view-driver">View Drivers</a></li>
            </ul>
        </li>
    <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "driver-map") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=driver-map"><i class="fa fa-map-marker" aria-hidden="true"></i> <span class="nav-label">Driver Map</span><span class="selected"></span></a></li>
      
      <!--add booking-->
        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "new_user_booking") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=new_user_booking"><i class="fa fa-taxi" aria-hidden="true"></i> <span class="nav-label" >Book A Ride</span><span class="selected"></span></a></li>

        <!--bookings-->
        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "bookings") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=bookings"><i class="fa fa-car" aria-hidden="true"></i> <span class="nav-label" >Rides</span><span class="selected"></span></a></li>
     <!--add booking-->
        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "pending_ride") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=pending_ride"><i class="fa fa-taxi" aria-hidden="true"></i> <span class="nav-label" >Pending Ride</span><span class="selected"></span></a></li>


        <!--bookings-->
        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "users") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=users"><i class="fa fa-user" aria-hidden="true"></i> <span class="nav-label" >Users</span><span class="selected"></span></a></li>

        <?php
        $li_open = $arr_open = $ul_open = "";
        if(@$_REQUEST['pages'] == "payment") {
            $li_open    = "active open";
            $arr_open   = "open";
            $ul_open    = "style='display: block'";
        }
        ?>
        <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=payment"><i class="fa fa-money" aria-hidden="true"></i> <span class="nav-label" >Payment</span><span class="selected"></span></a></li>


       <!-- Transactions Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "view-transactions") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-transactions"><i class="fa fa-user" aria-hidden="true"></i> <span class="nav-label" >Transactions</span><span class="selected"></span></a></li>
       <!-- user Start -->
      <?php
                            $li_open = $arr_open = $ul_open = "";
                            if(@$_REQUEST['pages'] == "ride-completed") {
                                $li_open    = "active open";
                                $arr_open   = "open";
                                $ul_open    = "style='display: block'";
                            }
                            ?>
      <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=ride-completed"><i class="fa fa-taxi" aria-hidden="true"></i> <span class="nav-label" >Ride Completed</span><span class="selected"></span></a></li>
      
    </ul>


  </nav>
</aside>
