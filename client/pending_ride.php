<?php
include_once '../apporioconfig/start_up.php';
include('common.php');

$company_id = $_SESSION['COMPANY']['ID'];
$query ="select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id where user.company_id='$company_id' AND ride_table.ride_status=1";
$result = $db->query($query);
$list = $result->rows;
$query ="select * from driver INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id where driver.online_offline = 1 and driver.status=1 and driver.busy=0 and driver.login_logout=1 and driver.company_id='$company_id'";
$result = $db->query($query);
$list1 = $result->rows;
?>
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Pending</h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Number</th>
                                        <th>Car</th>
                                        <th>Location</th>
                                        <th>Destination </th>
                                        <th>Driver</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $ride){ ?>
                                    <tr>
                                        <td>
                                            <?php
                                            $user_name = $ride['user_name'];
                                            echo $user_name;
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            $user_phone = $ride['user_phone'];
                                            echo $user_phone;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $car_type_name = $ride['car_type_name'];
                                            echo $car_type_name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $pickup_location = $ride['pickup_location'];
                                            echo $pickup_location;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $drop_location = $ride['drop_location'];
                                            echo $drop_location;
                                            ?>
                                        </td>
                                        <td> <button type="button" class="btn btn-success btn-xs" data-toggle="modal"  data-target="#assign<?php echo $ride['ride_id'];?>"  > Assign </button></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <?php foreach($list as $ride){ ?>
        <div class="modal fade"  id="assign<?php echo $ride['ride_id'];?>" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Assign Driver for Ride Id #<?php echo $ride['ride_id'];?></h4>
                        <div id="assign_driver_success" style="display:none;"> <br>
                            <div class="alert alert-succ$pages->driver_idess"> <strong>Success!</strong> Driver Assigned for Ride Id #<?php echo $ride['ride_id'];?> </div>
                        </div>
                    </div>
                    <div class="modal-body" >
                        <div class="tab-content">
                            <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                                <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Driver Name</th>
                                        <th>Assign</th>
                                    </tr>
                                    <?php foreach($list1 as $login)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $login['driver_id'];?></td>
                                            <td><?php echo $login['driver_name'];?></td>
                                            <td><a href="home.php?pages=bookings&status=8&driver_id=<?php echo $login['driver_id']?>&ride_id=<?php echo $ride['ride_id']?>" class="" title="Active">
                                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" >Assign</button>
                                                </a></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>