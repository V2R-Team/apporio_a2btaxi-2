<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['COMPANY']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
?>
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Payment Methods</h3>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table class="table table-striped table-bordered table-responsive">
                                    <tbody>
                                        <tr>
                                            <td><a href="home.php?pages=stripe"><img src="mastercard.png" alt="" width="319" height="86"></a></td>
                                            <td><a href="home.php?pages=paypal"><img src="visa.png" alt="" width="319" height="86"></a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="home.php?pages=braintree"><img src="braintree-logo.png" alt="" width="319" height="86"></a></td>
                                            <td><a href="home.php?pages=voguepay"><img src="logo.png" alt="" width="319" height="86"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body></html>