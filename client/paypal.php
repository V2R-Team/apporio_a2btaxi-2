<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['COMPANY']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; 
$paypalID = 'amir@apporio.com'; 
$query="select * from company_payment WHERE company_id='".$_SESSION['COMPANY']['ID']."'";
$result = $db->query($query);
$list = $result->row;

?>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Make Payment With Paypal</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class=" form" >
                       <form action="<?php echo $paypalURL; ?>" method="post">
                        <input type="hidden" name="business" value="<?php echo $paypalID; ?>">
                        <input type="hidden" name="cmd" value="_xclick">
                        <input type="hidden" name="item_name" value="Driver Subscription">
                        <input type="hidden" name="item_number" value="<?php echo $_SESSION['COMPANY']['ID'];?>">
                        
                        <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Amount</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="amount" value="<?php echo $list['payment'];?>"  readonly/>
                                </div>
                            </div>
                        <input type="hidden" name="currency_code" value="USD">
                        <input type='hidden' name='return' value='http://apporio.org/A2Btaxi/client/success.php'>
                         <input type='hidden' name='cancel_return' value='http://apporio.org/A2Btaxi/client/cancel.php'>
                         <input type='hidden' name='notify_url' value="http://apporio.org/A2Btaxi/client/ipn.php">
                         <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="image" name="submit" border="0" style="margin-top:20px !important;"
                              src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">
                              <img  alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
                                </div>
                            </div>
             </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</body>
</html></body></html>