<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['COMPANY']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$company_id = $_SESSION['COMPANY']['ID'];

$query="select * from user WHERE company_id='$company_id' ORDER BY user_id DESC";
$result = $db->query($query);
$list=$result->rows;


if(isset($_GET['status']) && isset($_GET['id']))
{
    $query1="UPDATE user SET status='".$_GET['status']."' WHERE user_id='".$_GET['id']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=users");
}

if(isset($_POST['savechanges']))
{
    $query2="UPDATE user SET user_name='".$_POST['user_name']."', user_email='".$_POST['user_email']."' , user_phone='".$_POST['user_phone']."'where user_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=users");
}
?>
<form method="post" name="frm">

    <input type="hidden" name="exchanges_value" value="" id="exchanges_value">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Users</h3>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th width="5%">S.No</th>
                                        <th>User Name</th>
                                        <th>Email Id</th>
                                        <th>Mobile No.</th>
                                        <th>Device</th>
                                        <th width="10%">Rating</th>
                                        <th width="8%">Status</th>
                                        <th width="12%">Full Details</th>
                                        <th width="4%">Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $user){?>
                                        <tr>
                                            <td><?php echo $user['user_id'];?></td>
                                            <td>
                                                <?php
                                                $user_name = $user['user_name'];
                                                    echo $user_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $user_email = $user['user_email'];
                                                    echo $user_email;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $user_phone = $user['user_phone'];
                                                    echo $user_phone;
                                                ?>
                                            </td>
                                            <td><?php
                                                $device = $user['flag'];
                                                if($device==1)
                                                {
                                                    echo "Iphone";
                                                }
                                                else if($device==2)
                                                {
                                                    echo "Android";
                                                }
                                                else
                                                {
                                                    echo "------";
                                                }
                                                ?></td>
                                            <td><?php
                                                $userrating = $user['rating'];
                                                if($userrating == 0){
                                                    echo "Not Rate Yet";
                                                }else {
                                                    $wholeStars = floor( $userrating );
                                                    $halfStar = round( $userrating * 2 ) % 2;
                                                    $HTML = "";
                                                    for( $i=0; $i<$wholeStars; $i++ ){
                                                        $HTML .= "<img src=star@13.png alt='Whole Star'>";
                                                    }
                                                    if( $halfStar ){
                                                        $HTML .= "<img src=halfstar.png alt='Half Star'>";
                                                    }

                                                    print $HTML;
                                                }?></td>
                                            <?php
                                            if($user['status']==1) {
                                                ?>
                                                <td class="text-center"><a href="home.php?pages=users&status=2&id=<?php echo $user['user_id']?>" class="" title="Active">
                                                        <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active </button>
                                                    </a></td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="text-center"><a href="home.php?pages=users&status=1&id=<?php echo $user['user_id']?>" class="" title="Deactive">
                                                        <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive </button>
                                                    </a></td>
                                            <?php } ?>

                                            <td><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#<?php echo $user['user_id'];?>"  > Full Details </button></td>
                                            <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#edit<?php echo $user['user_id'];?>"  ></button></td>
                                        </tr>
                                    <?php }?>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>
<!--User Details Starts-->
<?php
$dummyImg="http://apporio.co.uk/apporiotaxi/uploads/driver/driverprofile.png";
foreach($list as $user){?>
    <div class="modal fade" id="<?php echo $user['user_id'];?>" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content starts-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">User Full Details</h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                            <thead>
                            <tr>
                                <td class="" colspan="" style="text-align-last:left; width:150px;"><img src="<?php if($user['user_image'] != '' && isset($user['user_image'])){echo '../'.$user['user_image'];}else{ echo $dummyImg; }?>"  width="150px" height="150px"></td>
                                <td><table style="margin-left:20px;" aling="center" border="0">
                                        <tbody>
                                        <tr>
                                            <th class=""> Name</th>
                                            <td class=""><?php
                                                $user_name = $user['user_name'];
                                                    echo $user_name;
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <th class="">Email</th>
                                            <td class=""><?php
                                                $user_email = $user['user_email'];
                                                    echo $user_email;
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <th class="">Phone</th>
                                            <td class=""><?php
                                                $user_phone = $user['user_phone'];
                                                    echo $user_phone;
                                                ?></td>
                                        </tr>
                                        <tr>
                                            <th class="">Status</th>
                                            <td class=""><?php
                                                $login_logout = $user['login_logout'];
                                                if($login_logout==1)
                                                {
                                                    echo "<button class=\"btn btn-success btn-xs activebtn\">Login</button>";
                                                }
                                                else
                                                {
                                                    echo "<button class=\"btn btn-danger btn-xs\">Logout</button>";
                                                }
                                                ?></td>
                                        </tr>
                                        </tbody>
                                    </table></td>
                            </tr>
                            <tr>
                                <th class="">Rating</th>
                                <td class=""><?php
                                    $userrating = $user['rating'];
                                    if($userrating == 0){
                                        echo "Not Rate Yet";
                                    }else {
                                        $wholeStars = floor( $userrating );
                                        $halfStar = round( $userrating * 2 ) % 2;
                                        $HTML = "";
                                        for( $i=0; $i<$wholeStars; $i++ ){
                                            $HTML .= "<img src=star@13.png alt='Whole Star'>";
                                        }
                                        if( $halfStar ){
                                            $HTML .= "<img src=halfstar.png alt='Half Star'>";
                                        }

                                        print $HTML;
                                    }?></td>
                            </tr>
                            <tr>
                                <th class="">Device</th>
                                <td class=""><?php
                                    $phonedevice=$user['flag'];
                                    if($phonedevice==1)
                                    {
                                        echo "Iphone";
                                    }
                                    else if($phonedevice==2)
                                    {
                                        echo "Android";
                                    }
                                    else
                                    {
                                        echo "---------";
                                    }
                                    ?></td>
                            </tr>
                            <tr>
                                <th class="">Register Date</th>
                                <td class=""><?php
                                    $register_date = $user['register_date'];
                                        echo $register_date;
                                    ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Modal content closed-->

        </div>
    </div>
<?php }?>
<!--User Details Closed-->
<?php foreach($list as $user){?>
    <div class="modal fade" id="edit<?php echo $user['user_id'];?>" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content starts-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Users Details</h4>
                </div>
                <form  method="post"  onSubmit="return validatelogin()">
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">User Name</label>
                                    <input type="text" class="form-control"  placeholder="User Name" name="user_name" value="<?php echo $user['user_name'];?>" id="user_name" required>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">User Email</label>
                                    <input type="text" class="form-control"  placeholder="User Email" name="user_email" value="<?php echo $user['user_email'];?>" id="user_email" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">User Phone</label>
                                    <input type="text" class="form-control"  placeholder="User Phone" name="user_phone" value="<?php echo $user['user_phone'];?>" id="user_phone" required>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $user['user_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php }?>
<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>