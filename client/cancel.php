<?php
session_start();
include_once '../apporioconfig/start_up.php';
include('common.php');
$txn_id = $_GET['tx'];
$payment_gross = $_GET['amt'];
$currency_code = $_GET['cc'];
$payment_status = $_GET['st'];

?>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Payment Cancel</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class=" form" >   
                        <div class="form-group ">
                            
                                <div class="col-lg-10">
                                   <h1>Your PayPal transaction has been canceled.</h1>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</body>
</html></body></html>