<?php
	include "../apporioconfig/start_up.php";
	switch($_REQUEST['pages']) {
		case "dashboard" : include "dashboard.php"; break;
        case "logout" : include "logout.php"; break;
		case "index" : include "index.php"; break;
        case "add-driver" : include "add-driver.php"; break;
        case "view-driver" : include "view-driver.php"; break;
        case "book-now" : include "add-booking.php"; break;
        case "booking-form" : include "booking-form.php"; break;
		case "new_user_booking":include "new_user_booking.php";break;
        case "bookings" : include "ride-now.php"; break;
        case "users" : include "users.php"; break;
        case "payment" : include "payment.php"; break;
        case "payment_method" : include "payment_method.php"; break;
        case "stripe" : include "stripe.php"; break;
        case "paypal" : include "paypal.php"; break;
        case "braintree" : include "braintree.php"; break;
        case "voguepay" : include "voguepay.php"; break;
        case "change-password" : include "change-password.php";break;
        case "view-transactions" : include "view-transactions.php";break;
        case "ride-completed" : include "ride-completed.php";break;
        case "pending_ride" :include "pending_ride.php";break;
        case "driver-map" :include "driver-map.php";break;
		default : include "404.php";
	}



?>
