<?php
header("Content-Type: application/json");
class Account extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('Useraccountmodel');
        $this->load->model('Googlemodel');
    }

    public function Signup(){
        $first_name = $this->input->post('first_name');
        $lastname =  $this->input->post('last_name');
        $user_phone = $this->input->post('phone');
        $user_password = $this->input->post('password');
        if($first_name != "" && $lastname != "" && $user_phone != ""  && $user_password != ""){
             $user_name = $first_name." ".$lastname;
            $query = $this->Useraccountmodel->check_user($user_phone,$user_password);
            if($query == 0){
                $data = $this->Useraccountmodel->check_phone($user_phone);
                if($data == 0) {
                    $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                    $data=$dt->format('M j');
                    $day=date("l");
                    $date=$day.", ".$data ;
                    $data = array(
                        'user_name' => $user_name,
                        'user_phone' => $user_phone,
                        'user_password' => $user_password,
                        'register_date'=>$date
                    );
                    $text = $this->Useraccountmodel->signup($data);
                    $message = array('result' => 1, 'message' => "Signup Succusfully!!", 'details' => $text);
                }else{
                    $user_id = $data['user_id'];
                    $text = $this->Useraccountmodel->update_password($user_id,$user_password);
                    $message = array('result' => 1, 'message' => "Signup Succusfully!!", 'details' => $text);
                }
            }
            else{
                $message = array( 'result' =>0,'message' => "Phone already registerd!!");
            }
        }
        else{
            $message = array( 'result' =>0,'message' => "Required fields missing!!");
        }
        echo json_encode($message, JSON_PRETTY_PRINT);
    }

    public function phone()
    {
        $user_phone = $this->input->post('phone');
        if($user_phone != "")
        {

            $data = $this->Useraccountmodel->check_phone2($user_phone);
            if($data)
            {
                $message = array('result' => 1, 'message' => "user details", 'details' => $data);
            }else{
                $message = array( 'result' =>0,'message' => "phone number not register!!");
            }

        }else{
            $message = array( 'result' =>0,'message' => "Required fields missing!!");
        }
        echo json_encode($message, JSON_PRETTY_PRINT);
    }

    public function update_email()
    {
        $user_id = $this->input->post('user_id');
        $email = $this->input->post('email');
        if($user_id != "" && $email != "")
        {
            $data = $this->Useraccountmodel->update_email($email,$user_id);
            if($data)
            {
                $message = array( 'result' =>1,'message' => "email update",'details'=>$data);
            }else{
                $message = array( 'result' =>0,'message' => "wrong user id ");
            }
        }else{
            $message = array( 'result' =>0,'message' => "Required fields missing!!");
        }
        echo json_encode($message,JSON_PRETTY_PRINT);
    }

    public function Login(){
         $user_phone = $this->input->post('phone');
         $user_password = $this->input->post('password');
        if($user_phone != "" && $user_password != "" ){
            $query = $this->Useraccountmodel->login($user_phone,$user_password);  
            $id = $query['user_id'];
            $this->Useraccountmodel->online($id);
            if($query){
                $message = array( 'result' =>1,'message' => "Login Succusfully!!",'value'=>1,'details'=>$query);
            }
            else{

                $data = $this->Useraccountmodel->check_phone($user_phone);
                if($data)
                {
                    $pass = "";
                    $text = $this->Useraccountmodel->phone($user_phone,$pass);
                    if($text)
                    {
                        $message = array('result' => 0, 'message' => "password does not match!!", 'value' => 2);
                    }else {
                        $message = array('result' => 0, 'message' => "password does not match!!", 'value' => 0);
                    }
                }else {
                    $message = array('result' => 0, 'message' => "user does not exsit!!", 'value' => 4);
                }
            }
        }
        else{
            $message = array( 'result' =>0,'message' => "Required fields missing!!");
        }
        echo json_encode($message, JSON_PRETTY_PRINT);
    }

    public function Change_password()
    {
        $user_id = $this->input->post('user_id');
        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        if($user_id != "" && $old_password != "" && $new_password != ""){
            $data = $this->Useraccountmodel->change_password($user_id,$old_password,$new_password);
            if($data)
            {
                $message = array( 'result' =>1,'message' => "password updated");
            }else{
                $message = array( 'result' =>0,'message' => "old password wrong");
            }
        }else{
            $message = array( 'result' =>0,'message' => "Required fields missing!!");
        }
        echo json_encode($message,JSON_PRETTY_PRINT);
    }
    
    public function facebook_signup()
    {
        $facebook_id = $this->input->post('facebook_id');
        $facebook_mail = $this->input->post('facebook_mail');
	$facebook_image = $this->input->post('facebook_image');
        $facebook_firstname = $this->input->post('facebook_firstname');
        $facebook_lastname = $this->input->post('facebook_lastname');
        $phone = $this->input->post('phone');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
       
       if($facebook_id != "" && $facebook_mail != "" && $facebook_image != "" && $facebook_firstname != "" && $facebook_lastname != "" && $phone != "" && $first_name != "" && $last_name != "")
       {

         $data = $this->Useraccountmodel->check_phone($phone);
         if(empty($data))
         {
             $text = array(
                           'facebook_id'=>$facebook_id,
                           'facebook_mail'=>$facebook_mail,
                           'facebook_image'=>$facebook_image,
                           'facebook_firstname'=>$facebook_firstname,
                           'facebook_lastname'=>$facebook_lastname,
                           'user_phone'=>$phone,
                           'user_name' => $first_name." ".$last_name
                          );
                          
              $query = $this->Googlemodel->facebook_signup($text);
              
              $msg = array('result'=>1,'msg'=>"Facebook Signup Successfully",'details'=>$query);
         }else{
             $text = array(
                 'facebook_id'=>$facebook_id,
                 'facebook_mail'=>$facebook_mail,
                 'facebook_image'=>$facebook_image,
                 'facebook_firstname'=>$facebook_firstname,
                 'facebook_lastname'=>$facebook_lastname,
                 'user_name' => $first_name." ".$last_name
             );

               $query = $this->Googlemodel->facebook_updated($text,$phone);
              $msg = array('result'=>1,'msg'=>"Details Updated",'details'=>$query);
         }
       }else{
              $msg = array('result'=>0,'msg'=>"Required field Missing");   
         }
       echo json_encode($msg, JSON_PRETTY_PRINT);
    }
    
    public function facebook_login()
    {
      $facebook_id = $this->input->post('facebook_id');
      
      if($facebook_id != ""){
         $query = $this->Googlemodel->facebook_login($facebook_id);
         if(empty($query))
         {
            $msg = array('result'=>0,'msg'=>"Wrong Facebook Id");  
         }else{
              $msg = array('result'=>1,'msg'=>"login successfully",'details'=>$query);   
         }
         
      }else{
              $msg = array('result'=>0,'msg'=>"Required field Missing");   
         }
       echo json_encode($msg, JSON_PRETTY_PRINT);
    }
    
     public function google_signup()
    {
        $google_id = $this->input->post('google_id');
        $google_name = $this->input->post('google_name');
	$google_mail = $this->input->post('google_mail');
        $google_image = $this->input->post('google_image');
        $phone = $this->input->post('phone');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
       
       if($google_id != "" && $google_name != "" && $google_mail != "" && $google_image != "" && $phone != "" && $first_name != "" && $last_name != "")
       {

           $data = $this->Useraccountmodel->check_phone($phone);
       
         if(empty($data))
         {
             $text = array(
                           'google_id'=>$google_id,
                           'google_name'=>$google_name,
                           'google_mail'=>$google_mail,
                           'google_image'=>$google_image,
                           'user_phone'=>$phone,
                           'user_name' => $first_name." ".$last_name
                           );
                          
              $query = $this->Googlemodel->google_signup($text);
              
              $msg = array('result'=>1,'msg'=>"Google Signup Successfully",'details'=>$query);
         }else{
             $text = array(
                 'google_id'=>$google_id,
                 'google_name'=>$google_name,
                 'google_mail'=>$google_mail,
                 'google_image'=>$google_image,
                 'user_name' => $first_name." ".$last_name
                 );

             $query = $this->Googlemodel->google_update($text,$phone);
             $msg = array('result'=>1,'msg'=>"Detail updated",'details'=>$query);
         }
       }else{
              $msg = array('result'=>0,'msg'=>"Required field Missing");   
         }
       echo json_encode($msg, JSON_PRETTY_PRINT);
    }
    
     public function google_login()
    {
      $google_id = $this->input->post('google_id');
     
      if($google_id != ""){
         $query = $this->Googlemodel->google_login($google_id);
         if(empty($query))
         {
            $msg = array('result'=>0,'msg'=>"Wrong Google Id");  
         }else{
              $msg = array('result'=>1,'msg'=>"login successfully",'details'=>$query);   
         }
         
      }else{
              $msg = array('result'=>0,'msg'=>"Required field Missing");   
         }
       echo json_encode($msg, JSON_PRETTY_PRINT);
    }
    
   
}
