<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

$query="select * from payment INNER JOIN countries ON payment.country_id=countries.country_id";
$result = $db->query($query);
$list=$result->rows;

if(isset($_POST['savechanges']))
{
    $query2="UPDATE payment SET daily_price='".$_POST['daily_price']."',weekly_price='".$_POST['weekly_price']."',monthly_price='".$_POST['monthly_price']."' where payment_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=view-payment");
}


?>

<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View Payment Options</h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th width="5%">S.No</th>
                                        <th>Country Name</th>
                                        <th>Daily Price</th>
                                        <th>Weekly Price</th>
                                        <th>Monthly Price</th>
                                        <th width="4%">Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $payment){?>
                                        <tr>

                                            <td><?php echo $payment['payment_id'];?></td>
                                            <td><?php echo $payment['country_name']; ?></td>
                                            <td><?php $daily_price = $payment['daily_price'];
                                                    echo $daily_price;
                                                ?>
                                            </td>
                                            <td><?php $weekly_price = $payment['weekly_price'];
                                                    echo $weekly_price;
                                                 ?>
                                            </td>
                                            <td><?php $monthly_price = $payment['monthly_price'];
                                                    echo $monthly_price;
                                               ?>
                                            </td>
                                            <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#edit<?php echo $payment['payment_id'];?>"  ></button></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>

</form>

<!-- Page Content Ends -->
<?php foreach($list as $payment){?>
    <div class="modal fade" id="edit<?php echo $payment['payment_id'];?>" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content starts-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Rate Card Details</h4>
                </div>
                <form  method="post"  onSubmit="return validatelogin()">
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Daily Price</label>
                                    <input type="text" class="form-control"  placeholder="Daily Price" name="daily_price" value="<?php echo $payment['daily_price'];?>" id="daily_price" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Weekly Price</label>
                                    <input type="text" class="form-control"  placeholder="Weekly Minutes" name="weekly_price" value="<?php echo $payment['weekly_price'];?>" id="weekly_price" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Monthly Price</label>
                                    <input type="text" class="form-control"  placeholder="Monthly Price" name="monthly_price" value="<?php echo $payment['monthly_price'];?>" id="monthly_price" required>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $payment['payment_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>