<?php
include_once '../apporioconfig/start_up.php';
include('common.php');
function sortByOrder($a, $b)
{
    return $a['distance'] - $b['distance'];
}
$user_id=$_POST['user_id'];
$car_type_id=$_POST['car_type_id'];
$pickup_location=$_POST['pickup_location'];
$drop_location=$_POST['drop_location'];
 

            $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
			$data = $dt->format('M j');
			$day = date("l");
			$date = $day.", ".$data ;
			$time = date("h:i A");
             
                //pickup_location lat and long
                
        $address = str_replace(" ", "+", $pickup_location);
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response);
		$lat = $response_a->results[0]->geometry->location->lat;
		
		$long = $response_a->results[0]->geometry->location->lng;

                //drop_location lat and long
        $address1 = str_replace(" ", "+", $drop_location);
        $url1 = "http://maps.google.com/maps/api/geocode/json?address=$address1&sensor=false";
		$ch1 = curl_init();
		curl_setopt($ch1, CURLOPT_URL, $url1);
		curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch1, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 0);
		$response1 = curl_exec($ch1);
		curl_close($ch1);
		$response_a1 = json_decode($response1);
		$lat1 = $response_a1->results[0]->geometry->location->lat;
		$long1 = $response_a1->results[0]->geometry->location->lng;

$image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$lat.",".$long."&markers=color:red|label:D|".$lat1.",".$long1."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
$query="INSERT INTO ride_table (user_id,car_type_id,pickup_lat,pickup_long,pickup_location,drop_lat,drop_long,drop_location,ride_date,ride_time,ride_type,ride_status,status,ride_image) 
         VALUES ('$user_id','$car_type_id','$lat','$long','$pickup_location','$lat1','$long1','$drop_location','$date','$time',8,1,1,'$image')";
        $db->query($query);
        $ride_id = $db->getLastId();

        $query ="select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id where ride_table.ride_id='$ride_id'";
        $result = $db->query($query);
        $list = $result->row;

        $query ="select * from driver INNER JOIN car_type ON driver.car_type_id=car_type.car_type_id WHERE driver.online_offline=1 and driver.status=1 and driver.busy=0 and driver.login_logout=1";
        $result = $db->query($query);
        $list1 = $result->rows;

        ?>
<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Pending</h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Number</th>
                                        <th>Car</th>
                                        <th>Location</th>
                                        <th>Destination </th>
                                        <th>Driver</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <?php
                                                $user_name = $list['user_name'];
                                                    echo $user_name;
                                                ?>
                                            </td>

                                            <td>
                                                <?php
                                                $user_phone = $list['user_phone'];
                                                    echo $user_phone;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $car_type_name = $list['car_type_name'];
                                                    echo $car_type_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $pickup_location = $list['pickup_location'];
                                                    echo $pickup_location;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $drop_location = $list['drop_location'];
                                                 echo $drop_location;
                                                ?>
                                            </td>
                                            <td> <button type="button" class="btn btn-success btn-xs" data-toggle="modal"  data-target="#assign<?php echo $list['ride_id'];?>"  > Assign </button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <!-- Popup -->
            <div class="modal fade"  id="assign<?php echo $list['ride_id'];?>" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Assign Driver for Ride Id #<?php echo $list['ride_id'];?></h4>
                            <div id="assign_driver_success" style="display:none;"> <br>
                                <div class="alert alert-succ$pages->driver_idess"> <strong>Success!</strong> Driver Assigned for Ride Id #<?php echo $list['ride_id'];?> </div>
                            </div>
                        </div>
                        <div class="modal-body" >
                            <div class="tab-content">
                                <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Driver Name</th>
                                            <th>Assign</th>
                                        </tr>
                                        <?php foreach($list1 as $login)
                                        {
                                            ?>
                                            <tr>
                                                <td><?php echo $login['driver_id'];?></td>
                                                <td><?php echo $login['driver_name'];?></td>
                                                <td><a href="home.php?pages=ride-now&status=8&driver_id=<?php echo $login['driver_id']?>&ride_id=<?php echo $list['ride_id']?>" class="" title="Active">
                                                        <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" >Assign</button>
                                                    </a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="wraper container-fluid">
                <div class="page-title">
                    <h3 class="title">Driver Online</h3>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                        <table class="table table-striped table-bordered table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Driver Name</th>
                                                <th>Number</th>
                                                <th>Car type</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($list1 as $drivers):
                                                ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    echo $i;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $driver_name = $drivers['driver_name'];
                                                    echo $driver_name;
                                                    ?>
                                                </td>

                                                <td>
                                                    <?php
                                                    $driver_phone = $drivers['driver_phone'];
                                                    echo $driver_phone;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $car_type_name = $drivers['car_type_name'];
                                                    echo $car_type_name;
                                                    ?>
                                                </td>

                                                <td>
                                                    <button type="button" class="btn btn-success br2 btn-xs fs12" > Online </button>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                             endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

