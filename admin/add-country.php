<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');


if(isset($_POST['save']))
{
    $country  = $_POST['country'];
    $query2="INSERT INTO countries (country_name) VALUES ('$country')";
    $db->query($query2);
    $msg = "Country Added Succesfully";
    echo "<script>alert('".$msg."')</script>";
}

?>
<script type="text/javascript">
    function validatelogin() {
        var country = document.getElementById("country").value;
        if (country == "")
        {
            alert("Enter Country Name.");
            return false;
        }
    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Country</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Country Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Country Name" name="country" id="country">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
