<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
error_reporting(0);

date_default_timezone_set('Asia/Kolkata');
$timestamp = time();
$date_time = date("Y-m-d", $timestamp);




        if($_POST['driver_name']!=""){
          $where .= "AND driver_name like '%".$_POST['driver_name']."%'";
        }
        if($_POST['ride_date']!=""){
          $d = $_POST['ride_date'];
          if($d == 7){
            $new = date('Y-m-d', strtotime($date_time. ' - 7 days'));
          }else if($d == 30){
            $new = date('Y-m-d', strtotime($date_time. ' - 30 days'));
          }else if($d == 90){
            $new = date('Y-m-d', strtotime($date_time. ' - 90 days'));
          }else if($d == 180){
            $new = date('Y-m-d', strtotime($date_time. ' - 180 days'));
          }else if($d == 365){
            $new = date('Y-m-d', strtotime($date_time. ' - 365 days'));
          }

          $lastdate = "AND done_date BETWEEN  '".$new."' AND  '".$date_time."'  ";
        }

  $query="select * from driver WHERE 1 =1 $where ORDER BY driver_id DESC";
  $result = $db->query($query);
  $list=$result->rows;
?>


<!DOCTYPE html>
<html>
<head>
  <title></title>
  <style type="text/css">
    .searchtxt{
      width: 17%;
      margin:5px ;
      float: left;
    }
    .searchtxt1{
      width: 17%;
      margin-left:0px ;
      float: left;
    }
    .searchtxt1btn{
     background-color: rgba(121, 121, 121, 0.36);
    }
  </style>
</head>
<body>

<form method="post" name="frm">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">Accounts</h3>
   </div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        
        <div class="panel-body">
          <div class="row">
          <div class="form-group">
          <form role="form" method="post">
            Search By:
            <div class="clearfix"></div>
          <input type="text" name="driver_name" value="<?=$_REQUEST['driver_name']?>" placeholder="Name" class="searchtxt1 searchtxt form-control ">
          <select name="ride_date" class="searchtxt form-control ">
             <option>All</option>
            <option value ="7" >7 Days</option>
            <option value ="30">30 Days</option>
            <option value ="90">3 Months</option>
            <option value ="180">6 Months</option>
            <option value ="365">1 Year</option>
          </select>
          <input type="submit" name="Search" Value="Search" class="searchtxt  btn btn-info">
          </form>


          </div>
             <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                     
                    <th width="5%">S.No</th>
                    <th>Driver Name</th>
                    <th>Total Revenue</th>
                    <th>Commission Earned </th>
                    <th>Penalty</th>
                    <th>Total Money Received</th>
                    <th>Full Details</th>
                  </tr>
                </thead>
                <tbody>

                <?php foreach($list as $driver){?> 
                  <tr>
                    <td><?php echo $driver['driver_id'] ?></td>
                    <td><?php echo $driver['driver_name'] ?> </td>


                    <?php 
                  //  echo "select amount from done_ride WHERE driver_id = '".$driver['driver_id']."' ".$lastdate." ";

                     $query1="select amount from done_ride WHERE driver_id = '".$driver['driver_id']."' ".$lastdate." ";
                    $result1 = $db->query($query1);
                    $list1=$result1->rows;
                    
                    $total = "";
                    if(!empty($list1)){
                    foreach ($list1 as $key ) {
                        $total[] = $key['amount'];
                      $totals=array_sum($total);
                    }
                    
                  }else{
                    $totals= "0";
                  }
                    ?>


                    <td>Rs. <?php echo $totals; ?> </td>
                    <td>Rs. 1000 </td>
                    <td>Rs. 1000</td>

                    <?php 
//                    echo "select amount from done_ride WHERE driver_id = '".$driver['driver_id']."' ".$lastdate." ";

                     $query1="select amount from done_ride WHERE driver_id = '".$driver['driver_id']."' ";
                    $result1 = $db->query($query1);
                    $list1=$result1->rows;
                    
                    $total = "";
                    if(!empty($list1)){
                    foreach ($list1 as $key ) {
                        $total[] = $key['amount'];
                      $totals=array_sum($total);
                    }
                    
                  }else{
                    $totals= "0";
                  }
                    ?>

                    <td>Rs.<?php echo $totals; ?></td>

                    <td><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#<?php echo $driver['driver_id'];?>"  > Full Details </button></td>
                 

                <?php } ?>


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div>
</form>


<!-- Page Content Ends --> 
<!-- ================== -->


<?php foreach($list as $driver){?>

<div class="modal fade" id="<?php echo $driver['driver_id'];?>" role="dialog">
  <div class="modal-dialog modal-lg"> 
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-md-12"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
        <h4 class="modal-title fdetailsheading col-md-12 text-center">Transaction Details</h4>
      </div>
      <div class="modal-body">
        <div class="modal-body">
          <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
  <thead>
    <tr>
      <th class="">Ride Id</th>
      <th class="">Pickup Location</th>
      <th class="">Drop Location</th>
      <th class="">Transaction Id</th>
      <th class="">Payment Method</th>
      <th class="">Amount</th>
    </tr>
  </thead>
  <tbody>
      <?php 
        $query1="select * from done_ride LEFT JOIN payment_confirm ON done_ride.ride_id = payment_confirm.order_id WHERE done_ride.driver_id = '".$driver['driver_id']."' ";
        $result1 = $db->query($query1);
        $list1=$result1->rows;
        foreach ($list1 as $key ){
      ?>
    
    <tr>
        <td colspan="" class="">  
          <?php 
            $ride_id=$key['ride_id'];
              if($ride_id==''){
                echo "----";
              }else{
                echo $ride_id;  
              }
          ?>
        </td>
        <td colspan="" class="">  
          <?php 
            $begin_location=$key['begin_location'];
              if($begin_location==''){
                echo "----";
              }else{
                echo $begin_location;  
              }
          ?>
        </td>
        <td colspan="" class="">  
          <?php 
            $end_location=$key['end_location'];
              if($end_location==''){
                echo "----";
              }else{
                echo $end_location;  
              }
          ?>
        </td>
        <td colspan="" class="">  
          <?php 
            $payment_id=$key['payment_id'];
              if($payment_id==''){
                echo "----";
              }else{
                echo $payment_id;  
              }
          ?>
        </td>
        <td colspan="" class="">  
          <?php 
            $payment_method=$key['payment_method'];
              if($payment_method==''){
                echo "----";
              }else{
                echo $payment_method;  
              }
          ?>
        </td>
        <td colspan="" class="">  
          <?php 
            $payment_amount=$key['amount'];
              if($payment_amount==''){
                echo "----";
              }else{
                echo $payment_amount;  
              }
          ?>
        </td>
    </tr>
    <?php } ?>
   
  </tbody>
</table>

        </div>
      </div>
    </div>
    
    <!-- Modal content closed--> 
    
  </div>
</div>

<?php } ?>






</section>
<!-- Main Content Ends -->

</body></html>