<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

$query="select * from cancel_reasons";
	$result = $db->query($query);
	$list=$result->rows;       
        
   if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE cancel_reasons SET status='".$_GET['status']."' WHERE reason_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-cancel");
    }
    
    if(isset($_POST['delete'])) 
     {
      $query1="DELETE FROM cancel_reasons WHERE car_type_id='".$_POST['chk']."'";
      $db->query($query1);
      $db->redirect("home.php?pages=view-cancel");
     }
     
      if(isset($_POST['savechanges']))
	 
     {
	$query2="UPDATE cancel_reasons SET reason_name='".$_POST['reason_name']."', reason_name_french='".$_POST['reason_name_other']."' where reason_id='".$_POST['savechanges']."'";
	         $db->query($query2); 


 $db->redirect("home.php?pages=view-cancel");
	
	}
	
    
?>

<!-- Page Content Start -->
<!-- ================== -->
<form method="post" name="frm">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">View Cancel Reason</h3>

    <?php if($_SESSION['ADMIN']['ROLE'] == ""){ ?>
                  
    <button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>

    <?php }else{ 

      $query1="select * from role WHERE role_id = '".$_SESSION['ADMIN']['ROLE']."' ";
      $result1 = $db->query($query1);
      $list1=$result1->rows;
      foreach($list1 as $lists);
      $data =  json_decode(html_entity_decode($lists['role_permission']), true);
      if($data[cancel_reasons] == 1){
      ?>

      <button type="submit" name="delete" value="delete" class="btn btn-danger glyphicon glyphicon-trash"></button>

  <?php } } ?>

  </div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                  <?php if($_SESSION['ADMIN']['ROLE'] == ""){ ?>
                  
                  <th width="11%">&nbsp; Select</th>

                  <?php }else{ 

                    $query1="select * from role WHERE role_id = '".$_SESSION['ADMIN']['ROLE']."' ";
                    $result1 = $db->query($query1);
                    $list1=$result1->rows;
                    foreach($list1 as $lists);
                    $data =  json_decode(html_entity_decode($lists['role_permission']), true);
                    if($data[cancel_reasons] == 1){
                    ?>

                    <th width="11%">&nbsp; Select</th>

                <?php } } ?>

                    <th width="5%">S.No</th>
                    <th>Reason Name</th>
                     <th>Reason Name Other</th>
                     <th width="12%">Status</th>
                     <th width="4%">Edit</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($list as $cancelreason){?>
                  <tr>

                  <?php if($_SESSION['ADMIN']['ROLE'] == ""){ ?>
                  
                  <td>
                    <label class="option block mn" style="width: 55px;">
                       <input type="checkbox" name="chk" value="<?php echo $cancelreason['reason_id']?>" onClick="uncheck()" >
                       <span class="checkbox mn"></span>
                    </label>
                    </td>

                  <?php }else{ 

                    $query1="select * from role WHERE role_id = '".$_SESSION['ADMIN']['ROLE']."' ";
                    $result1 = $db->query($query1);
                    $list1=$result1->rows;
                    foreach($list1 as $lists);
                    $data =  json_decode(html_entity_decode($lists['role_permission']), true);
                    if($data[cancel_reason] == 1){
                    ?>

                    <td>
                    <label class="option block mn" style="width: 55px;">
                       <input type="checkbox" name="chk" value="<?php echo $cancelreason['reason_id']?>" onClick="uncheck()" >
                       <span class="checkbox mn"></span>
                    </label>
                    </td>

                <?php } } ?>

                    
                    <td><?php echo $cancelreason['reason_id'];?></td>
                   
                    <td>
                    <?php
            	      $reason_name=$cancelreason['reason_name'];
            	      if($reason_name=="")
            	      {
            	      echo "---------";
            	      }
            	      else
            	      {
            	       echo $reason_name;
            	      }
            	      ?>
            	      </td>
            	      
            	       <td>
                    <?php
            	      $reason_name_french=$cancelreason['reason_name_french'];
            	      if($reason_name_french=="")
            	      {
            	      echo "---------";
            	      }
            	      else
            	      {
            	       echo $reason_name_french;
            	      }
            	      ?>
            	      </td>
            	      
            	      
            	     
            	      
            	      
            	    <?php
                                if($cancelreason['status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-cancel&status=2&id=<?php echo $cancelreason['reason_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-cancel&status=1&id=<?php echo $cancelreason['reason_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                                <?php } ?>
         <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $cancelreason['reason_id'];?>"  ></button></td>           
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div> 
</form>
<?php foreach($list as $cancelreason){?>
<div class="modal fade" id="<?php echo $cancelreason['reason_id'];?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Reason Name</h4>
      </div>
      <form  method="post"  onSubmit="return validatelogin()">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Reason Name</label>
                <input type="text" class="form-control"  placeholder="Reason Name" name="reason_name" value="<?php echo $cancelreason['reason_name'];?>" id="reason_name" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Reason Name In French</label>
                <input type="text" class="form-control"  placeholder="Reason Name In French" name="reason_name_other" value="<?php echo $cancelreason['reason_name_french'];?>" 		   id="reason_name_other" required>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
          <button type="submit" name="savechanges" value="<?php echo $cancelreason['reason_id'];?>" class="btn btn-info">Save Changes</button>
        </div>
      </form>
    </div>
    
    <!-- Modal content closed--> 
    
  </div>
</div>
<?php }?>






<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>