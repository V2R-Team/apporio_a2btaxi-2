<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$query = "select * from countries WHERE country_status=1";
$result = $db->query($query);
$list = $result->rows;

if(isset($_POST['save']))
{
    $query="select * from company WHERE company_email='".$_POST['company_email']."'";
    $result = $db->query($query);
    $list=$result->row;
    if(count($list) == 0)
    {
        $query2="INSERT INTO company (company_name,company_email,company_phone,company_address,company_contact_person,company_password,country_id) VALUES ('".$_POST['company_name']."','".$_POST['company_email']."','".$_POST['company_phone']."','".$_POST['company_address']."','".$_POST['company_contact_person']."','".$_POST['company_password']."','".$_POST['country_id']."')";
        $db->query($query2);
        $errorMsg1 = "Company Details Save!";
    }else{
        $errorMsg = "Email already in Registerd!";
    }
}

?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwuW7ZtAG05nr_X5TKV4oHi7GBxeK7vDY&v=3.exp&libraries=places"></script>

<script>
    function initialize() {

        var input = document.getElementById('company_address');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script>
    function validatelogin() {
        var company_name = document.getElementById('company_name').value;
        var company_phone = document.getElementById('company_phone').value;
        var company_email = document.getElementById('company_email').value;
        var company_contact_person = document.getElementById('company_contact_person').value;
        var company_address = document.getElementById('company_address').value;
        var company_password = document.getElementById('company_password').value;
        var company_confirm_password = document.getElementById('company_confirm_password').value;
        var country_id = document.getElementById('country_id').value;
        if(company_name == "")
        {
            alert("Enter Company Name");
            return false;
        }
        if(company_phone == "")
        {
            alert("Enter Company Phone Number");
            return false;
        }
        if(company_email == "")
        {
            alert("Enter Company Email");
            return false;
        }
        if(company_contact_person == "")
        {
            alert("Enter Company Contact Person");
            return false;
        }
        if(company_address == "")
        {
            alert("Enter Company Address");
            return false;
        }
        if(company_password == "")
        {
            alert("Enter Company Default Password");
            return false;
        }
        if(company_confirm_password == "")
        {
            alert("Enter Confirm Password");
            return false;
        }
        if(company_password != company_confirm_password)
        {
            alert("Password And Confirm Password D'not Match")
            return false;
        }
        if(country_id == "")
        {
            alert("Select Country")
            return false;
        }
    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Company</h3>
        <?php if(isset($errorMsg)){ ?>
            <h3 style="float:right; color:red;">This Email already Registerd!</h3>
        <?php } ?>
        <?php if(isset($errorMsg1)){ ?>
            <h3 style="float:right; color:red;">Company Details Save!!</h3>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class=" form" >
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Company Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Company Name" name="company_name" id="company_name"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Company Phone</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Company Phone" name="company_phone" id="company_phone"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Company Email</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Company Email" name="company_email" id="company_email"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Contact Person</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Contact Person" name="company_contact_person" id="company_contact_person"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Company Address</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Company Address" name="company_address" id="company_address"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Choose Country*</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="country_id" id="country_id">
                                        <option value="">---Select Country--</option>
                                        <?php foreach($list as $item){ ?>
                                            <option value="<?php echo $item['country_id'];;?>"><?php echo $item['country_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Company Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" placeholder="Company Default Password" name="company_password" id="company_password"/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="lastname" class="control-label col-lg-2">Confirm Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" placeholder="Confirm Password" name="company_confirm_password" id="company_confirm_password"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</body>
</html>
