<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');


$query = "select * from countries";
$result = $db->query($query);
$list = $result->rows;


if(isset($_GET['status']) && isset($_GET['id']))
{
    $query1="UPDATE countries SET country_status='".$_GET['status']."' WHERE country_id='".$_GET['id']."'";
    $db->query($query1);
    $db->redirect("home.php?pages=view-city");
}


if(isset($_POST['savechanges']))
{
    $query2="UPDATE countries SET country_name='".$_POST['country_name']."' where country_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=view-country");
}

?>
<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View Countries</h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th width="5%">S.No</th>
                                        <th>Country Name</th>
                                        <th width="8%">Status</th>
                                        <th width="4%">Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $countries){?>
                                        <tr>


                                            <td><?php echo $countries['country_id'];?></td>
                                            <td>
                                                <?php
                                                $country_name = $countries['country_name'];
                                                    echo $country_name;
                                                ?>
                                            </td>
                                            <?php
                                            if($countries['country_status']==1) {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=view-city&status=2&id=<?php echo $countries['country_id']?>" class="" title="Active">
                                                        <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                                        </button></a>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="text-center">
                                                    <a href="home.php?pages=view-city&status=1&id=<?php echo $countries['country_id']?>" class="" title="Deactive">
                                                        <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                                        </button></a>
                                                </td>
                                            <?php } ?>
                                            <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $countries['country_id'];?>"  ></button></td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>

<?php foreach($list as $countries){?>
    <div class="modal fade" id="<?php echo $countries['country_id'];?>" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Country Details</h4>
                </div>
                <form  method="post"  onSubmit="return validatelogin()">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Country Name</label>
                                    <input type="text" class="form-control"  placeholder="Country Name" name="country_name" value="<?php echo $countries['country_name'];?>" id="country_name" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $countries['country_id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
<?php }?>
<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>