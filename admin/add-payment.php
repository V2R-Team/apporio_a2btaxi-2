<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

$query = "select * from countries WHERE country_status=1";
$result = $db->query($query);
$list = $result->rows;

if(isset($_POST['save'])) {
    $query1="INSERT INTO payment (country_id,daily_price,weekly_price,monthly_price) VALUES('".$_POST['country_id']."','".$_POST['daily_price']."','".$_POST['weekly_price']."','".$_POST['monthly_price']."')";
    $db->query($query1);
    $msg = "Details Save Succesfully";
    echo "<script>alert('".$msg."')</script>";
}

?>
<script type="text/javascript">

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

</script>
<script type="text/javascript">
    function Validate() {
        var country_id = document.getElementById("country_id").value;
        var daily_price = document.getElementById("daily_price").value;
        var weekly_price = document.getElementById("weekly_price").value;
        var monthly_price = document.getElementById("monthly_price").value;
        if (country_id == "")
        {
            alert("Select Country.");
            return false;
        }
        if (daily_price == "")
        {
            alert("Enter Daily Price.");
            return false;
        }
        if (weekly_price == "")
        {
            alert("Enter Weekly Price.");
            return false;
        }
        if (monthly_price == "")
        {
            alert("Enter Monthly Price.");
            return false;
        }
    }
</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Payment Card</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form" >
                        <form class="cmxform form-horizontal tasi-form"  method="post" onsubmit="return Validate()">

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Choose Country*</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="country_id" id="country_id">
                                        <option value="">---Select Country--</option>
                                        <?php foreach($list as $item){ ?>
                                            <option value="<?php echo $item['country_id'];;?>"><?php echo $item['country_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Daily Price*</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Daily Price*" name="daily_price" onkeypress="return isNumber(event)" id="daily_price">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Weekly Price*</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Weekly Price" name="weekly_price" onkeypress="return isNumber(event)" id="weekly_price">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Monthly Price*</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Monthly Price" name="monthly_price" onkeypress="return isNumber(event)" id="monthly_price" >
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save"  value="Add" >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</section>


</body>
</html>
