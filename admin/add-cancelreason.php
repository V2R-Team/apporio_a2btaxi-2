<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');

if(isset($_POST['save']))
     {


$con = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
$reason_name = mysqli_real_escape_string($con,$_POST['reason_name']);
$reason_name_other = mysqli_real_escape_string($con,$_POST['reason_name_other']);
$type = mysqli_real_escape_string($con,$_POST['reason_id']);


	$query2="INSERT INTO cancel_reasons (reason_name,reason_name_french,reason_type,status) VALUES ('".$reason_name."','".$reason_name_other."','".$type."',1)";
	$db->query($query2);

	}

?>

  <!-- Page Content Start -->
  <!-- ================== -->

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Cancel Reason</h3>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
              
              
              
               <div class="form-group ">
                  <label class="control-label col-lg-2">Please Select Type*</label>
                  <div class="col-lg-10">
                     <select class="form-control" name="reason_id" id="" required>
                        <option>--Please Select Reason For--</option>
                         <option value=2>Driver</option>
                         <option value=1>Customer</option>
                          
                    </select>




                  </div>
                </div>

              
              
              
              
              
              
                <div class="form-group ">
                  <label class="control-label col-lg-2">Reason Name*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="Add Reason Name" name="reason_name" id="reason_name" required>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Add Other Reason Name*</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control"  placeholder="Add Other Reason Name " name="reason_name_other" id="reason_name_other" required>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
