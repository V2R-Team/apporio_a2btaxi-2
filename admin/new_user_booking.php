<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$query1="select * from car_type WHERE status=1";
$result1 =$db->query($query1);
$list1=$result1->rows;
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>

<script>
    function initialize() {
        var input = document.getElementById('pickup_location');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    function initialize() {
        var input = document.getElementById('drop_location');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add New User Booking</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class=" form" >
                        <form class="cmxform form-horizontal tasi-form" action="new_user_booking_submit.php" method="post" >

                            <div class="form-group ">
                                <label for="user_f_name" class="control-label col-lg-2">Full Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Full Name" name="user_f_name" id="user_f_name" required/>
                                </div>
                            </div>


                            <div class="form-group ">
                                <label for="user_phone" class="control-label col-lg-2">Phone</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Phone" name="user_phone"  id="user_phone"  required/>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Car Type</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="car_type_id" id="car_type_id"  required>
                                        <option>--Please Select Vehicle--</option>
                                        <?php foreach($list1 as $carbrand){ ?>
                                            <option value="<?php echo $carbrand['car_type_id'];?>"><?php echo $carbrand['car_type_name']; ?></option>

                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group total green">
                                <label class="control-label col-lg-2">Pickup Location</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Current Location For Pickup" name="pickup_location" id="pickup_location">
                                </div>
                            </div>

                            <div class="form-group total green">
                                <label class="control-label col-lg-2">Drop Location</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Drop Location" name="drop_location"  id="drop_location">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="manual" value="Manual" >
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="auto" value="Auto" style="margin-left: 20px">
                                   </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->
                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->
</div>

<!-- Main Content Ends -->

</body>
</html>
