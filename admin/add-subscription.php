<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');


if(isset($_POST['save']))
{

    $query ="select * from subscriptions WHERE subscription_time='".$_POST['Subscriptions']."'";
    $result = $db->query($query);
    $list = $result->rows;
    if(count($list) == 0)
    {
        $query ="INSERT INTO subscriptions (subscription_time,subscription_amount) VALUES ('".$_POST['Subscriptions']."','".$_POST['subscription_amount']."')";
        $db->query($query);
        $errorMsg1 = "Subscriptions Details Save!";
    }else{
        $errorMsg = "Subscriptions pack already Added plz update!";
    }

}

?>

<script type="text/javascript">

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Subscriptions</h3>
        <?php if(isset($errorMsg)){ ?>
            <h3 style="float:right; color:red;">Subscriptions pack already Added plz update!</h3>
        <?php } ?>
        <?php if(isset($errorMsg1)){ ?>
            <h3 style="float:right; color:red;">Subscriptions Details Save!!</h3>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Subscriptions</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="Subscriptions" id="Subscriptions" required>
                                        <option value="">--Please Select Subscriptions Time--</option>
                                        <option value="Daily">Daily</option>
                                        <option value="Weekly">Weekly</option>
                                        <option value="Monthly">Monthly</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Amount</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control"  placeholder="Subscriptions Amount" name="subscription_amount" id="subscription_amount" onkeypress="return isNumber(event)" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
