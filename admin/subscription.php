<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');


$query="select * from company_payment INNER JOIN company ON company_payment.company_id=company.company_id";
$result = $db->query($query);
$list = $result->rows;
?>

<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Company Subscriptions</h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th width="5%">S.No</th>
                                        <th>Company Name</th>
                                        <th>Subscriptions</th>
                                        <th>Total Driver</th>
                                        <th>payment</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $subscriptions){?>
                                        <tr>
                                            <td><?php echo $subscriptions['company_payment_id'];?></td>
                                            <td>
                                                <?php
                                                $company_name = $subscriptions['company_name'];
                                                    echo $company_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $payment_id = $subscriptions['payment_id'];
                                                echo $payment_id;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $total_driver = $subscriptions['total_driver'];
                                                echo $total_driver;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $payment = $subscriptions['payment'];
                                                echo $payment;
                                                ?>
                                            </td>
                             
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

    </div>
</form>


<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>